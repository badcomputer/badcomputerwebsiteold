document.addEventListener('DOMContentLoaded', () => {
    const typedText = document.querySelector('.typed-text');
    const cursor = document.querySelector('.cursor');
    const inputElement = document.getElementById('input-element'); // Add an input element in your HTML

    // Focus the input element when tapping the cursor
    cursor.addEventListener('click', () => {
        inputElement.focus();
    });

    // Handle touch events for mobile devices
    document.addEventListener('touchstart', (event) => {
        const touchedElement = event.target;
    
        // Check if the touched element is not the input element or its parent
        if (touchedElement !== inputElement && !inputElement.contains(touchedElement)) {
            event.preventDefault(); // Prevent default touch behavior
            inputElement.blur(); // Blur the input element to remove focus
        }
    });


    document.addEventListener('keydown', (event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            const command = inputElement.value.trim(); // Change to inputElement.value
            executeCommand(command);
            inputElement.value = '';
            // cursor.style.display = 'none';
            typedText.textContent = ''; // Clear the typed text
        }
    });

    typeIt = true;

    function executeCommand(command) {
        const responseElement = document.createElement('div');
        responseElement.classList.add('response');
        let response = '';
    
        switch (command) {
            case 'help':
                response = 'Available commands:<br>discog - list all songs.<br>hello - meet & greet.<br>bad - everything gets worse.<br>about - about bad computer.' + 
                '<br>clear - clear text.';
                break;
            case 'discog':
                typeIt = false;
                response = 'Stereo Nicotine<br>Stereo Nicotine (VITICZ Remix)<br>Static Abyss<br>' +
                '4Me<br>New Dawn<br>Disarray<br>Silhouette (feat. Skyelle)<br>Paradigm (feat. KARRA)<br>' +
                'Voyage (feat. Savoi)<br>Sanctuary<br>Clarity<br>Riddle<br>2U<br>Chasing Clouds w/ Danyka Nadeau<br>' +
                'Truth<br>Silhouette (feat. Skyelle) [Feed Me Remix]<br>Infected Mushroom - Walking on the Moon (Bad Computer Remix)<br>' +
                'Somewhere New EP:<br>&nbsp;Destroy Me<br>&nbsp;Your Spell<br>&nbsp;Internet Love; 2010 (Intermission)<br>&nbsp;Paradise<br>&nbsp;Somewhere New<br>' +
                'Just Dance<br>Blue<br>Fly<br>Heartbeat<br>Infected Mushroom - Lies and Deceptions (Bad Computer Remix)<br>' +
                'Calling Us w/ David Feldman (feat. Jordan Grace)<br>Land of the Living w/ Bianca<br>...<br><br><br><br><br>' 
                break;
            case 'about':
                response = 'Bad Computer is an audiovisual art project run by Craig Ferrier out of Helsinki, Finland.';
                break;
            case 'hello':
                response = 'Hello, thank u for visiting me.';
                break;
            
            case 'clear':
                // Clear all previous responses
                const previousResponses = document.querySelectorAll('.response');
                previousResponses.forEach(response => response.remove());
                return; // Exit the function
            case 'bad':
                window.open('bad.html', '_blank');
                return;
            default:
                response = 'Command not found. Type "help" to list available commands.';
                break;
        }
        
        // Get the terminal container element
        const terminalContainer = document.querySelector('.terminal');
    
        // Remove old response elements if there are more than 9 responses
        const responseElements = terminalContainer.querySelectorAll('.response');
        if (responseElements.length >= 1) {
            // Remove the first (oldest) response element
            terminalContainer.removeChild(responseElements[0]);
        }
    
        // Initialize the response text to an empty string
        let currentText = '';
        let currentIndex = 0;
    
        // Function to type out the response text letter by letter
        function typeResponse() {
            if (currentIndex < response.length) {
                currentText += response[currentIndex];
                responseElement.innerHTML = currentText;
                currentIndex++;
                if (typeIt == true){
                    setTimeout(typeResponse, 25); // Adjust the delay as needed
                }else{
                    typeResponse()
                }
            }
        }
    
        typeResponse();
    
        document.querySelector('.terminal').appendChild(responseElement);
    
        // Clear the input and typed text
        inputElement.value = '';
        typedText.textContent = '';
    }
    

    document.addEventListener('touchstart', (event) => {
        event.preventDefault();
    });
});