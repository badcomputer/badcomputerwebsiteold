// typing-animation.js
const typingButtons = document.querySelectorAll('.typing-button');

function typeText(textElement, textToType) {
    let index = 0;

    function typeCharacter() {
        if (index < textToType.length) {
            textElement.textContent += textToType.charAt(index);
            index++;
            setTimeout(typeCharacter, 100); // Adjust typing speed (in milliseconds)
        }
    }

    typeCharacter();
}

// Start the typing animation for each button when the page loads
window.onload = () => {
    typingButtons.forEach(button => {
        const textToType = button.getAttribute('data-text');
        typeText(button, textToType);
    });
};